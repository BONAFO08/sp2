import "dotenv/config.js";
import redis from 'redis'
import bluebird from "bluebird";
bluebird.promisifyAll(redis);

const redisClient = redis.createClient({
    host: process.env.ELASTICACHE_URL,
    port: 6379
});

redisClient.on('error', (err) => {
    console.error(err);
});

export default redisClient;
