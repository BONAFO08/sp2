import  express  from "express";
const router = express.Router();

router.get("/health", (req, res) => {
    res.status(200).json({status : 'OK!'});
});

router.get("/", (req, res) => {
  res.redirect('https://www.delilahresto.cf/index/');
});

export {router}

