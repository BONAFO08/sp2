import { validatePayData } from '../controllers/dataVerify.js';
import { validateAdmin } from '../controllers/user.controller.js'
import { createPay,modifyAPay,deletePay} from '../controllers/pay.controller.js';

//Create a new payment
const newPay = (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        if (desToken.range == 'admin') {
            let pay = validatePayData(req.body, '', 'new')
            if (pay.boolean == true) {
                createPay(pay.data)
                    .then(resolve => res.status(200).send(resolve))
                    .catch(err => res.status(409).send(err));
            } else {
                res.status(400).send('Lo siento.Haz enviado datos invalidos.');
            }
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Modify a payment
const modPay = async (req, res) => {
    
    let desToken = validateAdmin(req.headers.authorization);
    
    if (desToken != false) {
        if (desToken.range == 'admin') {
            req.body['id'] = req.query.id;
            let pay = validatePayData(req.body, '', 'update');
            if (pay.name != false && pay.id != false) {
                let response = await modifyAPay(pay, pay.id);
                res.status(response.status).send(response.txt);
            } else {
                res.status(400).send('Lo siento.Haz enviado datos invalidos.');
            }
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
};



//Delete a payment
const delPay = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {
            let pay = validatePayData(req.query, '', 'update');
            if (pay.id != false) {
                let response = await deletePay(pay.id);
                res.status(response.status).send(response.txt);
            } else {
                res.status(400).send('Lo siento.Haz enviado datos invalidos.');
            }
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
};

export{
    newPay,
    modPay,
    delPay
}